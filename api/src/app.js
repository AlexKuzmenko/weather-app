const express = require('express')
const {connectDB} = require("./helpers/db.js")
const {port} = require("./configuration")

const app = express()
const cityRouter = require('./routers/city')
const {City} = require("./models/city")

app.use(express.json())
app.use(cityRouter)


function startServer() {
    app.listen(port, () => {
        console.log(`Server is listening on ${port}`)
    })


}

connectDB()
    .on('error', console.error.bind(console, 'connection error:'))
    .once("open", startServer)