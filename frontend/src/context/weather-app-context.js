import React from "react";

const WeatherAppContext = React.createContext(null);

export default WeatherAppContext;
