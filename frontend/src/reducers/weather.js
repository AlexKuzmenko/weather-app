const weatherReducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_WEATHER':
            return action.data
        default:
            return state
    }
}

export default weatherReducer;
