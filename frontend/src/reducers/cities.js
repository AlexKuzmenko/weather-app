const citiesReducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_CITIES':
            return action.cities
        case 'ADD_CITY':
            return [
                ...state,
                { _id: action._id, name: action.name }
            ]
        case 'REMOVE_CITY':
            return state.filter((city) => city._id !== action._id )
        default:
            return state
    }
}

export default citiesReducer;
