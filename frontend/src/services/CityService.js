const API_HOST = process.env.API_HOST || '';

const fetchCitiesAPI = async () => {
    let response = await fetch(API_HOST + "/api/cities");
    return await response.json();
}

const addCityAPI = async (city) => {
    let response = await fetch(API_HOST + '/api/cities', {
        method: 'POST', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(city),
    })
    return await response.json();
}

const deleteCityAPI = async (id) => {
    const response = await fetch(API_HOST + `/api/cities/${id}`, {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json'
        }
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

export {fetchCitiesAPI, addCityAPI, deleteCityAPI};