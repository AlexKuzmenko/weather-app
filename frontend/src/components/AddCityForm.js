import React, {useState, useContext} from "react";
import WeatherAppContext from "../context/weather-app-context";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {addCityAPI} from "../services/CityService"

import {Row, Col} from "react-bootstrap";

const AddCityForm = () => {
    const {dispatchCities} = useContext(WeatherAppContext);
    const [name, setName] = useState('')

    const addCity = (e) => {
        e.preventDefault()
        try {
            addCityAPI({name}).then((city) => {
                dispatchCities({
                    type: 'ADD_CITY',
                    name: city.name,
                    _id: city._id
                })
            })
            setName('')
        } catch (error) {
            console.log(error.message)
        }

    }

    return (
        <div className="border border-2 p-3 mb-3">
            <h3>Add city</h3>
            <Form onSubmit={addCity} className="mb-3">
                <Row>
                    <Col xs={12}>
                        <Form.Group className="mb-3" controlId="formCityName">
                            <Form.Control placeholder='City Name' value={name} onChange={(e) => setName(e.target.value)} />
                        </Form.Group>
                    </Col>
                    <Col xs={12} className='text-start'>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Col>
                </Row>


            </Form>
        </div>
    )
}

export default AddCityForm;
