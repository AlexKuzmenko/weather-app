import React, {useContext} from 'react';
import {ListGroupItem} from "react-bootstrap";
import WeatherAppContext from "../context/weather-app-context";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import {deleteCityAPI} from "../services/CityService";

const CityItem = ({ city }) => {
    const {dispatchCities, setCurrentCity} = useContext(WeatherAppContext);

    const clickHandle = (e) => {
        e.preventDefault();
        setCurrentCity(e.target.dataset.city);
    }

    const deleteCity = () => {
        try {
            deleteCityAPI(city._id).then(() => {
                dispatchCities({type: "REMOVE_CITY", _id: city._id})
            });
        } catch (error) {
            console.log(error.message)
        }

    }

    return (
        <ListGroupItem className='d-flex justify-content-between align-items-center'>
            <div>
                <h4>
                    <a href="#" onClick={clickHandle} data-city={city.name}>
                        {city.name}
                    </a>

                </h4>
            </div>
            <div>
                <FontAwesomeIcon onClick={deleteCity} icon={faTrash} />
            </div>
        </ListGroupItem>
    )
}

export default CityItem;
