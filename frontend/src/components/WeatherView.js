import {useContext} from "react";
import WeatherAppContext from "../context/weather-app-context";
import {ListGroup, ListGroupItem, Card} from "react-bootstrap";


const WeatherView = () => {
    const {weather} = useContext(WeatherAppContext);

    return (<>
            {weather && <div className="border border-2 p-3">
                <h3>Weather in {weather.name}</h3>
                <Card style={{ width: '50%', textAlign: 'center' }} className="m-auto">
                    <Card.Img style={{width: "100px", margin: "0 auto"}} variant="top" src={`http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`} />
                    <Card.Body>
                        <Card.Title>{weather.name}</Card.Title>
                        <ListGroup>
                            <ListGroupItem>
                                <strong>Temperature: </strong>{weather.main.temp}
                            </ListGroupItem>
                            <ListGroupItem>
                                <strong>Description: </strong>{weather.weather[0].main}
                            </ListGroupItem>
                        </ListGroup>
                    </Card.Body>
                </Card>
            </div>
            }
        </>

    );
}

export default WeatherView;
